#!/bin/sh

set -e

if [ -z "$AUTOPKGTEST_TMP" -o ! -d "$AUTOPKGTEST_TMP" ]; then
   echo '$AUTOPKGTEST_TMP not set or not pointing to a directory' 1>&2
   exit 1
fi

cp -pr tests/* "$AUTOPKGTEST_TMP"
cd "$AUTOPKGTEST_TMP"
sed -e 's:XTC="../src/xtermcontrol":XTC="/usr/bin/xtermcontrol":' -i xtermcontrol.sh
xvfb-run xterm -e '( ./xtermcontrol.sh ; echo $? ) | tee xtermcontrol.log'
cat xtermcontrol.log
exit $(tail -1 xtermcontrol.log)
